package learners;

import org.chicago.cases.Case3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Case3PolynomialFitter
{
    private static double featureSet[] = new double[10];

    private static double predictPrice(int time)
    {
        double sum = 0;

        for (int i = 0; i < featureSet.length; i++)
        {
            sum += time * Math.pow(featureSet[i], i);
        }

        return sum;
    }

    private static ArrayList<Double> pastPrices = new ArrayList<Double>();
    private static ArrayList<Double> trends = new ArrayList<Double>();

    private static double calcTrendSum(ArrayList<Double> data)
    {
        double sum = 0;

        for (int i = 0; i < data.size(); i++)
            sum += data.get(i);

        return sum;
    }

    public static void main(String args[]) throws Exception
    {

        for (int i = 0; i < featureSet.length; i++)
        {
            featureSet[i] = 1.5;
        }

        for (int i = 30; i < 31; i++)
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("samples/SimDataCase3_"+i+".csv")));

            String line;

            int time = 0;

            double currentTrend = 0;

            int WINDOW_SIZE = 10;
            int THRESHOLD = 4;

            ArrayList<Double> pastntrends = new ArrayList<Double>();
            pastPrices.add(100000.0);

            while ((line = in.readLine()) != null)
            {
                String tokens[] = line.split(",");

                double bid = Double.parseDouble(tokens[0]);
                double ask = Double.parseDouble(tokens[1]);

                double marketPrice = (bid + ask) / 2;

                double prevPrice = pastPrices.get(pastPrices.size() - 1);

                pastPrices.add(marketPrice);

                double trend = marketPrice > prevPrice ? 1.0 : -1.0;
                trends.add(trend);

                if (pastntrends.size() == WINDOW_SIZE)
                    pastntrends.remove(0);

                pastntrends.add(trend);

                if (calcTrendSum(pastntrends) >= THRESHOLD)
                    System.out.println("UPWARD TREND!!!");
                else if (calcTrendSum(pastntrends) <= -THRESHOLD)
                    System.out.println("DOWNWARD TREND!!!");
                else
                    System.out.println("NO TREND!!!");


            }
        }
    }
}
