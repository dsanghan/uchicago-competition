import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.chicago.cases.AbstractCase1;
import org.chicago.cases.Case1;
import org.chicago.cases.CaseObjects.Quote;
import sun.font.TrueTypeFont;


/*
* Sample Implementation.  This class is meant to serve as a scaffolding for your code only.  The class
* itself is what we consider a "JOB" in freeway.  This means it can be uploaded via the management console
* and executed as a functional set of code.  The code below "WORKS".  You can upload this file and test
* it for yourself.
*
* To get going on your own example, you can copy and paste this into your own file and subsequent Java class
* or you can simply fill in the gaps here, upload the job, and run it to see the changes take effect.
*
* The sole purpose of this job is to:
*
* 1)  Extend AbstractCase1, which makes it a runnable algo
* 2)  Return an implementation of the Case1 interface via the getCase1Implementation method
*
* You'll notice that our 'dummy' version of Case1 is implemented below.
*/
public class Caltech4Case1 extends AbstractCase1 {
	
	// Initialize an instance of my case that I will define below
	Case1 sampleCase = new Case1SampleImplementation();

	// Begin Implementation of case
	class Case1SampleImplementation implements Case1
    {
        NormalDistribution nd = new NormalDistribution(1, 0.05 * 0.05);
        private double sigma = 0.3;
        private double sigma_base = 0.3;
        private double e_t = 0;
        private double spread = 0.04;
        ArrayList<Double> pastVolatitiles = new ArrayList<Double>();
        double strikes[] = {80, 90, 100, 110, 120};
        public boolean initialize = true;
        double filled = 0;
        double total = 0;
        private double currTime = 0;
        private double VEGA_BASE = 0;
        private boolean ALLOW_ONLY_CLOSE = false;
        private boolean ALLOWED_TRADES[] = {true, true, true, true, true, true, true, true, true, true};

        HashMap<Double, ArrayList<Double>> orders = new HashMap<Double, ArrayList<Double>>();

        public void init()
        {
            for (int i = 0; i < strikes.length; i++)
            {
                orders.put(strikes[i], new ArrayList<Double>());
                orders.put(strikes[i] * -1, new ArrayList<Double>());
            }

            log(orders.keySet().toString());

            for (int i = 0; i < 5; i++)
                pastVolatitiles.add(0.3);

            VEGA_BASE = calculateVega(100, 100);

            initialize = false;
        }

        private void updateSigma()
        {
            currTime += 0.01;
            NormalDistribution sigmand = new NormalDistribution(0, currTime * currTime);
            double sample = sigmand.sample();
            sigma = sigma_base * Math.exp(-(0.05 * 0.05 * currTime)/2 + 0.05 * sample);
            log("Sigma: " + sigma + " Sigma Sample: " + sample);
            assert (pastVolatitiles.size() == 5);
            pastVolatitiles.remove(0);
            pastVolatitiles.add(sigma);
            e_t = nd.sample();
        }

        private double CNDF(double x)
        {
            int neg = (x < 0d) ? 1 : 0;
            if (neg == 1)
                x *= -1d;

            double k = (1d / (1d + 0.2316419 * x));
            double y = ((((1.330274429 * k - 1.821255978) * k + 1.781477937) * k - 0.356563782) * k + 0.319385130) * k;
            y = 1.0 - 0.398942280401 * Math.exp(-0.5 * x * x) * y;

            return (1d - neg) * y + neg * (1d - y);
        }

        private double blackScholes(double S,
                                    double K,
                                    double r,
                                    double T,
                                    double sigma)
        {
            double d_1 = (Math.log(S/K) + (r + (sigma * sigma) / 2) * T)/(sigma * Math.sqrt(T));
            double d_2 = d_1 - sigma * Math.sqrt(T);

            double price = CNDF(d_1) * S - CNDF(d_2) * K * Math.exp(-r * T);

            return price;
        }

        private double getPrice(double K, boolean ask)
        {
            double bs = blackScholes(100, K, 0.01, 1, sigma);

            //log("Sigma: " + sigma + " e_t: " + e_t + " Black Scholes: " + bs);

            return bs * (ask ? 1 + spread : 1 - spread) * e_t;
        }

        private void calculatePNL()
        {
            double PnL = 0;
            double openPnL = 0;

            NormalDistribution sigmand = new NormalDistribution(0, 1);
            double sample = sigmand.sample();
            sigma = sigma_base * Math.exp(-(0.05 * 0.05)/2 + 0.05 * sample);

            for (int i = 0; i < strikes.length; i++)
            {
                ArrayList<Double> sold = orders.get(strikes[i]);
                ArrayList<Double> bought = orders.get(strikes[i] * -1);

                int maxIndex = Math.min(sold.size(), bought.size());

                for (int j = 0; j < maxIndex; j++)
                {
                    PnL += sold.get(j) + bought.get(j);
                }

                double price = blackScholes(100, strikes[i], 0.01, 1, sigma);

                for (int j = maxIndex; j < Math.max(sold.size(), bought.size()); j++)
                {
                    if (j < sold.size())
                    {
                        openPnL += sold.get(j) - price;
                    }
                    else
                    {
                        openPnL += price + bought.get(j);
                    }
                }
            }

            log("Current orders: 80: " + orders.get(80.0).size() + ", " + orders.get(-80.0).size() + " " +
                    "90: " + orders.get(90.0).size() + ", " + orders.get(-90.0).size() + " " +
                    "100: " + orders.get(100.0).size() + ", " + orders.get(-100.0).size() + " " +
                    "110: " + orders.get(110.0).size() + ", " + orders.get(-110.0).size() + " " +
                    "120: " + orders.get(120.0).size() + ", " + orders.get(-120.0).size() + " ");
            log("Current closed PnL: " + PnL + " OpenPnL: " + openPnL + " Filled: " + filled + " Total: " + total);
        }

		/*
		 * Implements the corresponding method on the Case1 Interface
		 * 
		 * This example returns a quote for each strike in this case.  There is only one
		 * spread that we are tracking, and as you can see, that spread is returned for
		 * each strike and subsequent quote.  The newQuoteFill method will widen the spread
		 * each time we are hit and thus we will send wider values as long as we are still getting fills.
		 * 
		 * The getCurrentQuotes() method gets called when BrokerOrders enter the system.  This will happen
		 * at intervals.  The returned quotes will then be compared with the orders and any trades will be notified
		 * via the newQuoteFill method below.
		 */
		public Quote[] getCurrentQuotes() {
			log("My Case1 implementation received a request for current quotes");

            if (initialize)
                init();

            calculatePNL();
            updateSigma();
            verifyVega();
            total++;

            if (ALLOW_ONLY_CLOSE)
                updateAllowedTrades();
            else
                resetAllowedTrades();

            log("Trade State: " + ALLOWED_TRADES);

			return new Quote[] {
					new Quote(ALLOWED_TRADES[0] ? getPrice(80, false) : 0, ALLOWED_TRADES[1] ? getPrice(80, true) : 1000, 80),
					new Quote(ALLOWED_TRADES[2] ? getPrice(90, false) : 0, ALLOWED_TRADES[3] ? getPrice(90, true) : 1000, 90),
					new Quote(ALLOWED_TRADES[4] ? getPrice(100, false) : 0, ALLOWED_TRADES[5] ? getPrice(100, true) : 1000, 100),
					new Quote(ALLOWED_TRADES[6] ? getPrice(110, false) : 0, ALLOWED_TRADES[7] ? getPrice(110, true) : 1000, 110),
					new Quote(ALLOWED_TRADES[8] ? getPrice(120, false) : 0, ALLOWED_TRADES[9] ? getPrice(120, true) : 1000, 120)
			};
		}

        public void updateAllowedTrades()
        {
            for (int i = 0; i < strikes.length; i++)
            {
                ArrayList<Double> sold = orders.get(strikes[i]);
                ArrayList<Double> bought = orders.get(strikes[i] * -1);

                if (sold.size() == bought.size())
                {
                    ALLOWED_TRADES[2 * i] = false;
                    ALLOWED_TRADES[2 * i + 1] = false;
                }

                else if (sold.size() > bought.size())
                {
                    ALLOWED_TRADES[2 * i] = true;
                    ALLOWED_TRADES[2 * i + 1] = false;
                }

                else
                {
                    ALLOWED_TRADES[2 * i] = false;
                    ALLOWED_TRADES[2 * i + 1] = true;
                }
            }
        }

        public void resetAllowedTrades()
        {
            ALLOWED_TRADES = new boolean[] {true, true, true, true, true, true, true, true, true, true};
        }

        private double computeDelta(double S, double K)
        {
            double sigma = 0, count = 0;

            for (int i = 0; i < pastVolatitiles.size(); i++, count++)
            {
                sigma += pastVolatitiles.get(i);
            }

            sigma /= count;

            double d_1 = (Math.log(S/K) + (0.01 + (sigma * sigma) / 2))/(sigma);

            return CNDF(d_1);
        }

        private double calculateVega(double S, double K)
        {
            double sigma = 0, count = 0, vega = 0;

            for (int i = 0; i < pastVolatitiles.size(); i++, count++)
            {
                sigma += pastVolatitiles.get(i);
            }

            sigma /= count;

            double d_1 = (Math.log(S/K) + (0.01 + (sigma * sigma) / 2))/(sigma);

            double pdf = Math.exp(-(d_1 * d_1  / 2)) / Math.sqrt(2 * Math.PI);

            return S * pdf;
        }

        private void verifyVega()
        {
            double vegaSum = 0;

            for (int i = 0; i < strikes.length; i++)
            {
                int sold = orders.get(strikes[i]).size();
                int bought = orders.get(strikes[i] * -1).size();

                vegaSum += Math.abs(sold - bought) * calculateVega(100, strikes[i]);
            }

            if (vegaSum >= 4 * VEGA_BASE)
            {
                ALLOW_ONLY_CLOSE = true;
            }
            else
                ALLOW_ONLY_CLOSE = false;

            log("VEGA EXCEEDED: " + ALLOW_ONLY_CLOSE + " Vega: " + vegaSum + " Max Vega: " + (5 * VEGA_BASE));
        }

		// Implements the corresponding method on the Case1 Interface
		// This implementation widens the spread and logs a message
		public int newQuoteFill(double fillPrice, double strike, int sign) {
			log("My logic received a quote Fill, price=" + fillPrice + ", strike=" + strike + ", sign=" + sign);

            //spread = Math.min(0.4, spread + 0.01);

            double actualPrice = blackScholes(100, strike, 0.01, 1, sigma);


            ArrayList<Double> temp = orders.get(sign * strike);
            temp.add(sign * fillPrice);
            filled++;

            double delta = computeDelta(100, strike);
            sigma_base = sigma;
            currTime = 0;

            if (sign == 1)
                delta -= 1;

            //spread = 0.04;

			return (int) (delta * 100);
		}

		// Implements the corresponding method on the Case1 Interface
		// This method simply logs a message saying we did not get a fill.
		public void brokerNotFilled() {
			//log("No match against broker the broker orders...time to adjust some levers?");
            updateSpread();
            //spread = Math.min(0.4, spread - 0.01);
		}

        private void updateSpread()
        {
            //spread -= 0.005;
        }

		
	}

	/*
	 *  This method is called by the system to get a reference to your version of Case1.  It then
	 *  uses this reference to run your logic.
	 */
	public Case1 getCase1Implementation() {
		return sampleCase;
	}

}
