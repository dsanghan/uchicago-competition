import org.chicago.cases.AbstractCase3;
import org.chicago.cases.Case3;
import org.ejml.simple.SimpleMatrix;

import java.util.ArrayList;


/*
 * Sample Implementation.  This class is meant to serve as a scaffolding for your code only.  The class
 * itself is what we consider a "JOB" in freeway.  This means it can be uploaded via the management console
 * and executed as a functional set of code.  The code below "WORKS".  You can upload this file and test
 * it for yourself.
 * 
 * To get going on your own example, you can copy and paste this into your own file and subsequent Java class
 * or you can simply fill in the gaps here, upload the job, and run it to see the changes take effect.
 * 
 * The sole purpose of this job is to:
 * 
 * 1)  Extend AbstractCase3, which makes it a runnable algo
 * 2)  Return an implementation of the Case3 interface via the getCase3Implementation method
 * 
 * You'll notice that our 'dummy' version of Case3 is implemented below.
 */
public class Caltech4Case3 extends AbstractCase3 {

    // Initialize an instance of my case that I will define below
    Case3 sampleCase = new MITCase3Implementation();

    enum trend {UPWARD, DOWNWARD, NONE};

    // Begin Implementation of case
    class MITCase3Implementation implements Case3 {


        int response = 1;
        int t = 1;
        int posSum = 0;

        private int NUM_STATES = 2;
        private int NUM_OBSERVATIONS = 1000;
        private boolean EMISSION = true;
        private int TREND_THRESHOLD = 7;
        private int WINDOW_SIZE = 13;
        private trend previousTrend = trend.NONE;

        private double Adata[][] = {{0.8331585748116997, 0.1668414251883005}, {0.16334975469998206, 0.8366502453000177}};
        private double Bdata[][]= {{0.5372740574609005, 0.20289903931195719, 0.1309793122633565, 0.06810534876120941, 0.06074224220257707},
                {0.06230984086213535, 0.06820422764697262, 0.13077822616079707, 0.19905343452561766, 0.5396542708044777}};
        private double Cdata[][] = {{0.16541051566596024, 0.83458948433404}, {0.842654903527041, 0.1573450964729592}};

        private double Sdata[][] = {{1.0, 0.0}};

        private double emission1[][] = {{-10}, {-5}, {0}, {5}, {10}};
        private double emission2[][] = {{1}, {3}};

        private double marketPrice = 100000;

        private double alphas[][] = new double[NUM_STATES][NUM_OBSERVATIONS];

        private ArrayList<Double> trends = new ArrayList<Double>();

        private double sums[] = new double[5];
        private double sums_e2[] = new double[2];

        SimpleMatrix A = new SimpleMatrix(Adata);
        SimpleMatrix B = new SimpleMatrix(Bdata);
        SimpleMatrix C = new SimpleMatrix(Cdata);
        SimpleMatrix S = new SimpleMatrix(Sdata);
        SimpleMatrix E1 = new SimpleMatrix(emission1);
        SimpleMatrix E2 = new SimpleMatrix(emission2);

        double expected_e1 = 0;
        double expected_e2 = 0;

        private int getIndex(double observation)
        {
            return EMISSION ? (int)(observation / 5) + 2 : (int)((observation - 1)/2);
        }

        private void updateAlphas(double e1, double e2)
        {
            EMISSION = false;

            if (t == 1)
            {
                alphas[0][0] = 1.0;
                alphas[1][0] = 0.0;
            }

            for (int j = 0; j < NUM_STATES; j++)
            {
                for (int i = 0; i < NUM_STATES; i++)
                {
                    int obs = getIndex(e2);
                    alphas[j][t] += alphas[i][t-1] * Adata[i][j] * Cdata[j][obs];
                }
            }

            double coeffcient = 1.0 / (alphas[0][t] + alphas[1][t]);
            alphas[0][t] *= coeffcient;
            alphas[1][t] *= coeffcient;

            double temp[][] = {{alphas[0][t], alphas[1][t]}};
            SimpleMatrix temp2 = new SimpleMatrix(temp);

            t++;

            EMISSION = true;

            S = temp2.mult(A);
        }

        private double calcTrendSum(ArrayList<Double> data)
        {
            double sum = 0;

            for (int i = 0; i < data.size(); i++)
                sum += data.get(i);

            return sum;
        }

        private int time = 0;
        private double cashSum = 0;
        private double marketMin = 100000;
        private double marketMax = 100000;
        private double tradePrice = 0;

        /*
           * Implements the corresponding method on the Case3 Interface
           *
           * This example simply returns the response integer.  When a fill comes in, it will toggle between
           * negative and positive.  Since the return type of this case determines whether I'm buying or selling,
           * my algo with then toggle between accumulating and selling positions.
           */
        public int newBidAsk(double bidPrice, double askPrice) {
            //log("I received a new Top-of-Book update, bid=" + bidPrice + ", offer=" + askPrice);

            double marketPriceNew = (bidPrice + askPrice) / 2;
            double e1 = marketPriceNew - marketPrice;
            double e2 = (askPrice - bidPrice) / 2;

            marketPrice = marketPriceNew;

            marketMin = Math.min(marketPrice, marketMin);
            marketMax = Math.max(marketPrice, marketMax);

            //sums[getIndex(e1)] += e1;
            //sums_e2[(int)((e2 - 1)/2)] += e2;

            if (trends.size() == WINDOW_SIZE)
                trends.remove(0);

            trends.add(e1 > 0 ? 1.0 : -1.0);

            double direction = calcTrendSum(trends);

            trend currTrend = direction > TREND_THRESHOLD ?  trend.UPWARD :
                    direction < -TREND_THRESHOLD ? trend.DOWNWARD :
                            trend.NONE;

            System.out.println("Bid: " + bidPrice + " Ask: " + askPrice +
                    " E1: " + e1 + " Expected E1: " + expected_e1 +
                    " E2: " + e2 + " Expected E2: " + expected_e2 + " Position: " +
                    posSum + " Trend: " + currTrend + " Direction: " + direction + " Cash: " + cashSum + " : " + marketMin + " : " + marketMax);

            //System.out.println(marketPrice + ", " + direction + ", " + currTrend);

            switch(currTrend)
            {
                case UPWARD:
                    if (previousTrend == trend.UPWARD && posSum < 1)
                    {
                        if (posSum == -1)
                        {
                            if (tradePrice - marketPrice < 0)
                                return 0;
                        }
                        posSum += 1;
                        previousTrend = currTrend;
                        return 1;
                    }
                    break;
                case DOWNWARD:
                    if (previousTrend == trend.DOWNWARD && posSum > -1)
                    {
                        if (posSum == 1)
                        {
                            if (marketPrice - tradePrice < 0)
                                return 0;
                        }
                        posSum -= 1;
                        previousTrend = currTrend;
                        return -1;
                    }
                    break;
                case NONE:
                    switch (previousTrend)
                    {
                        case UPWARD:
                            if (posSum > -1)
                            {
                                if (posSum == 1)
                                {
                                    if (marketPrice - tradePrice < 0)
                                        return 0;
                                }
                                posSum -= 1;
                                previousTrend = currTrend;
                                return -1;
                            }
                        case DOWNWARD:
                            if (posSum < 1)
                            {
                                if (posSum == -1)
                                {
                                    if (tradePrice - marketPrice < 0)
                                        return 0;
                                }
                                posSum += 1;
                                previousTrend = currTrend;
                                return 1;
                            }
                        case NONE:
                            previousTrend = currTrend;
                            return 0;
                    }
                    previousTrend = currTrend;
                    break;
                default:
                    previousTrend = currTrend;
                    return 0;
            }
            previousTrend = currTrend;
            return 0;
            /*
            System.out.println(e1 + ", " + expected_e1 + ", " + e2 + ", " + expected_e2);

            if (t != NUM_OBSERVATIONS)
                updateAlphas(e1, e2);
            else
                printStats();

            SimpleMatrix p_e1 = S.mult(B);
            expected_e1 = p_e1.mult(E1).get(0,0);
            SimpleMatrix p_e2 = S.mult(C);
            expected_e2 = p_e2.mult(E2).get(0,0);

            marketPrice = marketPriceNew;

			return expected_e1 < -0.4875 ? -1 : expected_e1 > 0.4625 ? 1 : 0; */
        }

        private void printStats()
        {
            System.out.println("E1: ----------------------------------");
            for (int i = 0; i < sums.length; i++)
            {
                System.out.print(sums[i]/NUM_OBSERVATIONS + " ");
            }
            System.out.println();
            System.out.println("--------------------------------------");

            System.out.println("E2: ----------------------------------");
            for (int i = 0; i < sums_e2.length; i++)
            {
                System.out.print(sums_e2[i]/NUM_OBSERVATIONS + " ");
            }
            System.out.println();
            System.out.println("--------------------------------------");
        }

        // Implements the corresponding method on the Case3 Interface
        // This implementation toggles the response integer and logs a message
        public void newOrderFill(double fillPrice, long volume) {
            //log("I received new fill, price=" + fillPrice + ", quantity=" + volume);
            cashSum += -volume * fillPrice;
            tradePrice = -volume * fillPrice;
        }

    }

    /*
      *  This method is called by the system to get a reference to your version of Case3.  It then
      *  uses this reference to run your logic.
      */
    public Case3 getCase3Implementation() {
        return sampleCase;
    }

}
