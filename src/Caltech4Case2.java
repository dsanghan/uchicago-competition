import java.util.Random;

import org.chicago.cases.AbstractCase2;
import org.chicago.cases.Case2;
import org.chicago.cases.CaseObjects.OrderInfo;



/*
 * Sample Implementation.  This class is meant to serve as a scaffolding for your code only.  The class
 * itself is what we consider a "JOB" in freeway.  This means it can be uploaded via the management console
 * and executed as a functional set of code.  The code below "WORKS".  You can upload this file and test
 * it for yourself.
 * 
 * To get going on your own example, you can copy and paste this into your own file and subsequent Java class
 * or you can simply fill in the gaps here, upload the job, and run it to see the changes take effect.
 * 
 * The sole purpose of this job is to:
 * 
 * 1)  Extend AbstractCase2, which makes it a runnable algo
 * 2)  Return an implementation of the Case2 interface via the getCase2Implementation method
 * 
 * You'll notice that our 'dummy' version of Case2 is implemented below.
 */
public class Caltech4Case2 extends AbstractCase2 {
	
	// Initialize an instance of my case that I will define below
	Case2 sampleCase = new Case2SampleImplementation();
	Random random = new Random();

	// Begin Implementation of case
	class Case2SampleImplementation implements Case2 {
		
		int response = 1;

		/*
		 * Implements the corresponding method on the Case2 Interface
		 * 
		 * This example simply returns a new OrderInfo array of size 1.  The OrderInfo says to
		 * place an order for the product I received a market update for, at the bidPrice, with a quantity
		 * of (response * randomNumber(0-5)).  Since response toggles between a negative and positive
		 * multiplier, you will alternate between buying and selling.
		 */
		public OrderInfo[] newBidAsk(String idSymbol, double bidPrice, double askPrice) {
			log("I received a new Top-of-Book update, bid=" + bidPrice + ", offer=" + askPrice);
			return new OrderInfo[] {new OrderInfo(idSymbol, (response * random.nextInt(5)), bidPrice)};
		}

		// Implements the corresponding method on the Case2 Interface
		// This implementation toggles the response integer and logs a message
		public void newOrderFill(String idSymbol, double fillPrice, long volume) {
			log("I received new fill, sym=" + idSymbol + ", price=" + fillPrice + ", quantity=" + volume);
			response *= -1;
		}
		
	}

	/*
	 *  This method is called by the system to get a reference to your version of Case2.  It then
	 *  uses this reference to run your logic.
	 */
	public Case2 getCase2Implementation() {
		return sampleCase;
	}

}
