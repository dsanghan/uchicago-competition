import org.apache.commons.math3.distribution.NormalDistribution;

public class Case1Support
{
    private double CNDF(double x)
    {
        int neg = (x < 0d) ? 1 : 0;
        if (neg == 1)
            x *= -1d;

        double k = (1d / (1d + 0.2316419 * x));
        double y = ((((1.330274429 * k - 1.821255978) * k + 1.781477937) * k - 0.356563782) * k + 0.319385130) * k;
        y = 1.0 - 0.398942280401 * Math.exp(-0.5 * x * x) * y;

        return (1d - neg) * y + neg * (1d - y);
    }

    private double blackScholes(double S,
                                double K,
                                double r,
                                double T,
                                double sigma)
    {
        double d_1 = (Math.log(S/K) + (r + (sigma * sigma) / 2) * T)/(sigma * Math.sqrt(T));
        double d_2 = d_1 - sigma * Math.sqrt(T);

        double price = CNDF(d_1) * S - CNDF(d_2) * K * Math.exp(-r * T);

        return price;
    }

    public static void main(String args[]) throws Exception
    {
        Case1Support cs = new Case1Support();

        for (double i = 0.01; i <= 1; i += 0.01)
        {
            NormalDistribution nd = new NormalDistribution(0, 1);
            double sample = nd.sample();
            double sigma = 0.3 * Math.exp(-(0.05 * 0.05 * 1)/2 + 0.05 * sample);
            System.out.println(sigma);
        }

        for (double i = 0.25; i <= 0.35; i += 0.01)
            System.out.println(cs.blackScholes(100, 80, 0.05, 1, i));
    }
}
