import org.chicago.cases.Case3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Case3Tester
{
    public static void main(String args[]) throws Exception
    {
        for (int i = 30; i < 31; i++)
        {
            Caltech4Case3 c3 = new Caltech4Case3();
            Case3 c = c3.getCase3Implementation();

            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("samples/SimDataCase3_"+i+".csv")));

            String line;

            while ((line = in.readLine()) != null)
            {
                String tokens[] = line.split(",");

                double bid = Double.parseDouble(tokens[0]);
                double ask = Double.parseDouble(tokens[1]);

                c.newBidAsk(bid, ask);
            }
        }
    }
}
