import org.ejml.simple.SimpleMatrix;

import javax.activation.ActivationDataFlavor;
import java.io.*;
import java.util.ArrayList;

public class Case3Learner
{
    private static int NUM_OBSERVATIONS = 1000;
    private static int NUM_STATES = 2;
    private static boolean EMISSION = true;
    private static boolean LOG = false;

    private static void readFromFile(String filename, double Adata[][], double Bdata[][]) throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

        String Aline1[] = br.readLine().split(" ");
        String Aline2[] = br.readLine().split(" ");
        br.readLine();
        String Bline1[] = br.readLine().split(" ");
        String Bline2[] = br.readLine().split(" ");

        for (int i = 0; i < Adata[0].length; i++)
        {
            Adata[0][i] = Double.parseDouble(Aline1[i]);
            Adata[1][i] = Double.parseDouble(Aline2[i]);
        }

        for (int i = 0; i < Bdata[0].length; i++)
        {
            Bdata[0][i] = Double.parseDouble(Bline1[i]);
            Bdata[1][i] = Double.parseDouble(Bline2[i]);
        }
    }

    private static void outputToFile(double Adata[][],  double Bdata[][], int index) throws Exception
    {
        String filename = EMISSION ? "samplePredictions/HMM_predictions_e1_"+index+".txt" : "samplePredictions/HMM_predictions_e2_"+index+".txt";
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename)));

        for (int i = 0; i < Adata.length; i++)
        {
            String output = "";
            for (int j = 0; j < Adata[i].length; j++)
            {
                output += Adata[i][j] + " ";
            }
            output += "\n";
            out.write(output);
        }

        out.write("-------------------------\n");

        for (int i = 0; i < Bdata.length; i++)
        {
            String output = "";
            for (int j = 0; j < Bdata[i].length; j++)
            {
                output += Bdata[i][j] + " ";
            }
            output += "\n";
            out.write(output);
        }

        out.close();
    }

    private static void printArray(double[][] array)
    {
        if (!LOG)
            return;

        System.out.println("-------------------------------");
        System.out.print("{");
        for (int i = 0; i < array.length; i++)
        {
            System.out.print("{");
            for (int j = 0; j < array[i].length; j++)
            {
                System.out.print(array[i][j]);
                if (j != array[i].length - 1)
                    System.out.print(", ");
                else
                    System.out.print("}");
            }
            System.out.println(i == array.length - 1 ? "}" : "");
        }
        System.out.println("-------------------------------");
    }

    private static double[][] deepClone(double input[][])
    {
        double result[][] = new double[NUM_STATES][NUM_OBSERVATIONS];

        for (int i = 0; i < NUM_STATES; i++)
            result[i] = input[i].clone();

        return result;
    }


    private static int getIndex(double observation)
    {
        return EMISSION ? (int)(observation / 5) + 2 : (int)((observation - 1)/2);
    }

    public static double[][] updateAlphas(double alphas[][], double Adata[][], double Bdata[][], double observations[])
    {
        for (int t = 1; t < NUM_OBSERVATIONS; t++)
        {
            for (int j = 0; j < NUM_STATES; j++)
            {
                for (int i = 0; i < NUM_STATES; i++)
                {
                    int obs = getIndex(observations[t]);
                    alphas[j][t] += alphas[i][t-1] * Adata[i][j] * Bdata[j][obs];
                }
            }

            //double coeffcient = 1.0 / (alphas[0][t] + alphas[1][t]);
            //alphas[0][t] *= coeffcient;
            //alphas[1][t] *= coeffcient;

            System.out.println(alphas[0][t] + " : " + alphas[1][t]);
        }

        return alphas;
    }

    public static double[][] updateBetas(double betas[][], double Adata[][], double Bdata[][], double observations[])
    {
        for (int t = NUM_OBSERVATIONS - 2; t >= 0; t--)
        {
            for (int j = 0; j < NUM_STATES; j++)
            {
                for (int i = 0; i < NUM_STATES; i++)
                {
                    int obs = getIndex(observations[t]);
                    betas[j][t] += betas[i][t+1] * Adata[i][j] * Bdata[j][obs];
                }
            }

            //double coeffcient = 1.0 / (betas[0][t] + betas[1][t]);
            //betas[0][t] *= coeffcient;
            //betas[1][t] *= coeffcient;
        }

        return betas;
    }

    public static void updateGamas(double gamas[][][],
                                   double Adata[][],
                                   double Bdata[][],
                                   double alphas[][],
                                   double betas[][],
                                   double observations[])
    {
        for (int t = NUM_OBSERVATIONS - 2; t >= 0; t--)
        {
            for (int i = 0; i < NUM_STATES; i++)
            {
                for (int j = 0; j < NUM_STATES; j++)
                {
                    gamas[i][j][t] = alphas[i][t] * Adata[i][j] * Bdata[j][getIndex(observations[t])] * betas[j][t+1];
                    System.out.println(gamas[i][j][t]);
                }
            }
        }
    }

    private static double Adata[][] = {{0.75, 0.25}, {0.25, 0.75}};
    private static double Bdata[][] = {{0.4, 0.2, 0.2, 0.1, 0.1},
            {0.1, 0.1, 0.2, 0.2, 0.4}};
    private static double Cdata[][] = {{0.25, 0.75}, {0.75, 0.25}};

    private static void runFileE1(String filename, int index) throws Exception
    {
        EMISSION = true;
        Adata = new double[][]{{0.75, 0.25}, {0.25, 0.75}};
        Bdata = new double[][]{{0.4, 0.2, 0.2, 0.1, 0.1},
        {0.1, 0.1, 0.2, 0.2, 0.4}};

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

        String line = "";
        double prevMarketPrice = 100000;

        double alphas[][] = new double[NUM_STATES][NUM_OBSERVATIONS];
        alphas[0][0] = 1;
        alphas[1][0] = 0;

        double e1Data[] = new double[NUM_OBSERVATIONS];
        double e2Data[] = new double[NUM_OBSERVATIONS];
        int count = 0;

        while ((line = br.readLine()) != null)
        {
            String tokens[] = line.split(",");

            double bid = Double.parseDouble(tokens[0]);
            double ask = Double.parseDouble(tokens[1]);

            double marketPrice = (bid + ask) / 2;
            double e1 = prevMarketPrice - marketPrice;
            double e2 = (bid - ask) / 2;

            e1Data[count] = e1;
            e2Data[count++] = e2;

            //System.out.println(bid + " " + ask + " " + marketPrice + " " + e1 + " " + e2);
            prevMarketPrice = marketPrice;
        }

        double betas[][] = new double[NUM_STATES][NUM_OBSERVATIONS];
        betas[0][NUM_OBSERVATIONS - 1] = Bdata[0][getIndex(e1Data[NUM_OBSERVATIONS - 1])];
        betas[1][NUM_OBSERVATIONS - 1] = Bdata[1][getIndex(e1Data[NUM_OBSERVATIONS - 1])];

        alphas = updateAlphas(deepClone(alphas), Adata, Bdata, e1Data);
        betas = updateBetas(deepClone(betas), Adata, Bdata, e1Data);

        double gamas[][][] = new double[NUM_STATES][NUM_STATES][NUM_OBSERVATIONS];

        updateGamas(gamas, Adata, Bdata, alphas, betas, e1Data);

        /*
            Updating A
         */

        for (int i = 0; i < NUM_STATES; i++)
        {
            double denom = 0;

            for (int j = 0; j < NUM_STATES; j++)
            {
                for (int t = 0; t < NUM_OBSERVATIONS - 1; t++)
                {
                    denom += gamas[i][j][t];
                }
            }

            for (int j = 0; j < NUM_STATES; j++)
            {
                double numerator = 0;

                for (int t = 0; t < NUM_OBSERVATIONS - 1; t++)
                {
                    numerator += gamas[i][j][t];
                }

                Adata[i][j] = numerator / denom;
            }
        }

        if (LOG)
            printArray(Adata);

        /*
            Update B
         */

        double e1P[] = {-10, -5, 0, 5, 10};

        for (int j = 0; j < NUM_STATES; j++)
        {
            for (int k = 0; k < 5; k++)
            {
                double denom = 0;
                double numer = 0;

                for (int i = 0; i < NUM_STATES; i++)
                {
                    for (int t = 0; t < NUM_OBSERVATIONS - 1; t++)
                    {
                        denom += gamas[i][j][t];
                        numer += e1P[k] == e1Data[t] ? gamas[i][j][t] : 0;
                    }
                }

                Bdata[j][k] = numer / denom;
            }
        }

        if (LOG)
            printArray(Bdata);

        outputToFile(Adata, Bdata, index);
    }

    private static void runFileE2(String filename, int index) throws Exception
    {
        EMISSION = false;
        Adata = new double[][]{{0.75, 0.25}, {0.25, 0.75}};
        Bdata = new double[][]{{0.25, 0.75}, {0.75, 0.25}};

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

        String line = "";
        double prevMarketPrice = 100000;

        double alphas[][] = new double[NUM_STATES][NUM_OBSERVATIONS];
        alphas[0][0] = 1;
        alphas[1][0] = 0;

        double e1Data[] = new double[NUM_OBSERVATIONS];
        double e2Data[] = new double[NUM_OBSERVATIONS];
        int count = 0;

        while ((line = br.readLine()) != null)
        {
            String tokens[] = line.split(",");

            double bid = Double.parseDouble(tokens[0]);
            double ask = Double.parseDouble(tokens[1]);

            double marketPrice = (bid + ask) / 2;
            double e1 = prevMarketPrice - marketPrice;
            double e2 = (ask - bid) / 2;

            e1Data[count] = e1;
            e2Data[count++] = e2;

            //System.out.println(bid + " " + ask + " " + marketPrice + " " + e1 + " " + e2);
            prevMarketPrice = marketPrice;
        }

        double betas[][] = new double[NUM_STATES][NUM_OBSERVATIONS];
        betas[0][NUM_OBSERVATIONS - 1] = Bdata[0][getIndex(e2Data[NUM_OBSERVATIONS - 1])];
        betas[1][NUM_OBSERVATIONS - 1] = Bdata[1][getIndex(e2Data[NUM_OBSERVATIONS - 1])];

        alphas = updateAlphas(deepClone(alphas), Adata, Bdata, e2Data);
        betas = updateBetas(deepClone(betas), Adata, Bdata, e2Data);

        double gamas[][][] = new double[NUM_STATES][NUM_STATES][NUM_OBSERVATIONS];

        updateGamas(gamas, Adata, Bdata, alphas, betas, e2Data);

        /*
            Updating A
         */

        for (int i = 0; i < NUM_STATES; i++)
        {
            double denom = 0;

            for (int j = 0; j < NUM_STATES; j++)
            {
                for (int t = 0; t < NUM_OBSERVATIONS - 1; t++)
                {
                    denom += gamas[i][j][t];
                }
            }

            for (int j = 0; j < NUM_STATES; j++)
            {
                double numerator = 0;

                for (int t = 0; t < NUM_OBSERVATIONS - 1; t++)
                {
                    numerator += gamas[i][j][t];
                }

                Adata[i][j] = numerator / denom;
            }
        }

        if (LOG)
            printArray(Adata);

        /*
            Update B
         */

        double e2P[] = {1, 3};

        for (int j = 0; j < NUM_STATES; j++)
        {
            for (int k = 0; k < e2P.length; k++)
            {
                double denom = 0;
                double numer = 0;

                for (int i = 0; i < NUM_STATES; i++)
                {
                    for (int t = 0; t < NUM_OBSERVATIONS - 1; t++)
                    {
                        denom += gamas[i][j][t];
                        numer += e2P[k] == e2Data[t] ? gamas[i][j][t] : 0;
                    }
                }

                Bdata[j][k] = numer / denom;
            }
        }

        if (LOG)
            printArray(Bdata);

        outputToFile(Adata, Bdata, index);
    }

    public static void main(String args[]) throws Exception
    {
        for (int i = 0; i < 30; i++)
        {
            String filename = "samples/SimDataCase3_" + i + ".csv";
            runFileE1(filename, i);
            runFileE2(filename, i);
        }

        LOG = true;

        double Afinal[][] = new double[2][2];
        double Bfinal[][] = new double[2][5];
        double Cfinal[][] = new double[2][2];

        for (int i = 0; i < 30; i++)
        {
            String filename = "samplePredictions/HMM_predictions_e1_"+i+".txt";
            double A[][] = new double[2][2];
            double B[][] = new double[2][5];
            double C[][] = new double[2][2];
            System.out.println("Reading: " + filename);
            readFromFile(filename, A, B);

            for (int j = 0; j < Afinal.length; j++)
                for (int k = 0; k < Afinal[j].length; k++)
                    Afinal[j][k] += A[j][k];

            for (int j = 0; j < Bfinal.length; j++)
                for (int k = 0; k < Bfinal[j].length; k++)
                    Bfinal[j][k] += B[j][k];

            filename = "samplePredictions/HMM_predictions_e2_"+i+".txt";
            System.out.println("Reading: " + filename);
            readFromFile(filename, A, C);

            for (int j = 0; j < Afinal.length; j++)
                for (int k = 0; k < Afinal[j].length; k++)
                    Afinal[j][k] += A[j][k];

            for (int j = 0; j < Cfinal.length; j++)
                for (int k = 0; k < Cfinal[j].length; k++)
                    Cfinal[j][k] += C[j][k];

        }

        for (int j = 0; j < Afinal.length; j++)
            for (int k = 0; k < Afinal[j].length; k++)
                Afinal[j][k] /= 60;

        for (int j = 0; j < Bfinal.length; j++)
            for (int k = 0; k < Bfinal[j].length; k++)
                Bfinal[j][k] /= 30;

        for (int j = 0; j < Cfinal.length; j++)
            for (int k = 0; k < Cfinal[j].length; k++)
                Cfinal[j][k] /= 30;

        printArray(Afinal);
        printArray(Bfinal);
        printArray(Cfinal);
    }
}
